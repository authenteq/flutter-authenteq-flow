
class IdentificationResult {
  String verificationId;
  String? selfieImageFilePath;
  String? proofOfAddressFilePath;
  List<DocumentIdentificationResult>? documents;

  IdentificationResult(this.verificationId);

  static IdentificationResult fromMap(dynamic map) {
    var result = IdentificationResult(map['verificationId']);
    result.selfieImageFilePath = map['selfieImageFilePath'];
    result.proofOfAddressFilePath = map['proofOfAddressFilePath'];
    result.documents = map['documents']?.map<DocumentIdentificationResult>(
            (val) => DocumentIdentificationResult.fromMap(val)
    ).toList();
    return result;
  }
}

enum DocumentIdentificationType {
  // ignore: constant_identifier_names
  PASSPORT,
  // ignore: constant_identifier_names
  DRIVERS_LICENSE,
  // ignore: constant_identifier_names
  NATIONAL_ID
}

class DocumentIdentificationResult {
  DocumentIdentificationType documentType;
  String? givenNames;
  String? surname;
  String? surnameAndGivenNames;
  DateTime? dateOfBirth;
  DateTime? dateOfExpiry;
  DateTime? dateOfIssue;
  String? sex;
  String? nationality;
  String? issuingCountry;
  String? documentNumber;
  String? address;
  String? placeOfBirth;
  String? issuingAuthority;
  String? personalNumber;
  String? givenNamesLocalized;
  String? surnameAndGivenNamesLocalized;
  String? placeOfBirthLocalized;
  String? issuingAuthorityLocalized;
  String? addressLocalized;
  String? jurisdiction;
  String? licenseClass;
  Map<String,DriversLicenseClassDetailResult>? licenseClassDetails;
  bool? isSixteenPlus;
  bool? isEighteenPlus;
  bool? isTwentyOnePlus;
  String? documentFrontImageFilePath;
  String? documentBackImageFilePath;

  DocumentIdentificationResult(this.documentType);

  static DocumentIdentificationResult fromMap(dynamic map) {
    var documentType = DocumentIdentificationType.PASSPORT;
    switch (map['documentType']) {
      case 'DL':
        documentType = DocumentIdentificationType.DRIVERS_LICENSE;
        break;
      case 'NID':
        documentType = DocumentIdentificationType.NATIONAL_ID;
        break;
    }
    var result = DocumentIdentificationResult(documentType);
    result.givenNames = map['givenNames'];
    result.surname = map['surname'];
    result.dateOfBirth = map['dateOfBirth'] == null ? null : DateTime.parse(map['dateOfBirth']);
    result.dateOfExpiry = map['dateOfExpiry'] == null ? null : DateTime.parse(map['dateOfExpiry']);
    result.dateOfIssue = map['dateOfIssue'] == null ? null : DateTime.parse(map['dateOfIssue']);
    result.sex = map['sex'];
    result.nationality = map['nationality'];
    result.issuingCountry = map['issuingCountry'];
    result.documentNumber = map['documentNumber'];
    result.address = map['address'];
    result.placeOfBirth = map['placeOfBirth'];
    result.issuingAuthority = map['issuingAuthority'];
    result.personalNumber = map['personalNumber'];
    result.givenNamesLocalized = map['givenNamesLocalized'];
    result.surnameAndGivenNamesLocalized = map['surnameAndGivenNamesLocalized'];
    result.placeOfBirthLocalized = map['placeOfBirthLocalized'];
    result.issuingAuthorityLocalized = map['issuingAuthorityLocalized'];
    result.addressLocalized = map['addressLocalized'];
    result.jurisdiction = map['jurisdiction'];
    result.licenseClass = map['licenseClass'];
    result.licenseClassDetails = map['licenseClassDetails']?.map<String,DriversLicenseClassDetailResult>(
            (k,v) => MapEntry(k as String, DriversLicenseClassDetailResult.fromMap(v))
    );
    result.isSixteenPlus = map['isSixteenPlus'] == true;
    result.isEighteenPlus = map['isEighteenPlus'] == true;
    result.isTwentyOnePlus = map['isTwentyOnePlus'] == true;
    return result;
  }
}

class DriversLicenseClassDetailResult {
  DateTime? from;
  DateTime? to;
  String? notes;

  static DriversLicenseClassDetailResult fromMap(dynamic map) {
    var result = DriversLicenseClassDetailResult();
    result.from = map['from'] == null ? null : DateTime.parse(map['from']);
    result.to = map['to'] == null ? null : DateTime.parse(map['to']);
    result.notes = map['notes'];
    return result;
  }
}
