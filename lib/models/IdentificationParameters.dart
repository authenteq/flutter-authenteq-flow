
class IdentificationParameters {
  /// Value for ClientID (required)
  String clientId = '';

  /// Value for ClientSecret
  String? clientSecret;

  /// Value for Flow ID (if clientSecret is used)
  String? flowId;

  /// Value for authentication token as alternative to ClientSecret
  String? token;

  // UI Customization settings
  Map<String,dynamic>? theme;
}
