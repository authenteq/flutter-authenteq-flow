
class FaceAuthenticationParameters {
  /// Value for ClientID (required)
  String clientId = '';

  /// Value for ClientSecret
  String? clientSecret;

  /// Value for authentication token as alternative to ClientSecret
  String? token;

  // UI Customization settings
  Map<String,dynamic>? theme;

  /// VerificationID to check (required)
  String verificationId = '';
}
