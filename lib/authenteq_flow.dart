
import 'dart:async';

import 'package:flutter/services.dart';
import 'models/IdentificationParameters.dart';
import 'models/IdentificationResult.dart';
import 'models/FaceAuthenticationParameters.dart';

class AuthenteqFlow {
  static const MethodChannel _channel = MethodChannel('authenteq_flow');

  /// Obtain Authenteq SDK version
  static Future<String> get getVersion async {
    final String version = await _channel.invokeMethod('getVersion');
    return version;
  }

  /// Flutter plugin to connect Authenteq Mobile SDK for identification flow
  static Future<IdentificationResult> identification(IdentificationParameters parameters) async {
    final dynamic result = await _channel.invokeMethod('identification', <String, dynamic>{
      'clientId': parameters.clientId,
      'clientSecret': parameters.clientSecret,
      'flowId': parameters.flowId,
      'token': parameters.token,
      'theme': parameters.theme
    });
    return IdentificationResult.fromMap(result);
  }

  /// Flutter plugin to connect Authenteq Mobile SDK for face authentication
  static Future<String> faceAuthentication(FaceAuthenticationParameters parameters) async {
    final dynamic result = await _channel.invokeMethod('faceAuthentication', <String, dynamic>{
      'clientId': parameters.clientId,
      'clientSecret': parameters.clientSecret,
      'token': parameters.token,
      'theme': parameters.theme,
      'verificationId': parameters.verificationId
    });
    return result['code'];
  }
}
