package com.authenteq.flow.plugin.authenteq_flow;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.authenteq.android.flow.result.DocumentIdentificationResult;
import com.authenteq.android.flow.result.DriversLicenseIdentificationResult;
import com.authenteq.android.flow.result.IdCardIdentificationResult;
import com.authenteq.android.flow.result.IdentificationResult;
import com.authenteq.android.flow.result.PassportIdentificationResult;
import com.authenteq.android.flow.ui.faceauthentication.FaceAuthenticationActivity;
import com.authenteq.android.flow.ui.faceauthentication.FaceAuthenticationParams;
import com.authenteq.android.flow.ui.faceauthentication.FaceAuthenticationParamsWithClientSecret;
import com.authenteq.android.flow.ui.faceauthentication.FaceAuthenticationParamsWithToken;
import com.authenteq.android.flow.ui.identification.IdentificationActivity;
import com.authenteq.android.flow.ui.identification.IdentificationParams;
import com.authenteq.android.flow.ui.identification.IdentificationParamsWithClientSecret;
import com.authenteq.android.flow.ui.identification.IdentificationParamsWithToken;

import org.threeten.bp.LocalDate;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;

/** AuthenteqFlowPlugin */
public class AuthenteqFlowPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware, PluginRegistry.ActivityResultListener {
  private MethodChannel channel;
  private Activity activity;
  private Result result;

  private static final int REQUEST_CODE_IDENTIFICATION = 1001;
  private static final int REQUEST_CODE_FACE_AUTHENTICATION = 1002;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "authenteq_flow");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    switch (call.method) {
      case "getVersion":
        String value = com.authenteq.android.flow.BuildConfig.VERSION_NAME;
        result.success(value);
        break;
      case "identification":
        identification(call, result);
        break;
      case "faceAuthentication":
        faceAuthentication(call, result);
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  void identification(MethodCall call, Result result) {
    String clientId = call.argument("clientId");
    String clientSecret = call.argument("clientSecret");
    String flowId = call.argument("flowId");
    String token = call.argument("token");
    int themeResource = getStyle(null);
    if (call.argument("theme") != null) {
      Map theme = call.argument("theme");
      if (theme != null && theme.get("AndroidStyle") != null) {
        themeResource = getStyle((String)theme.get("AndroidStyle"));
      }
    }

    try {
      if (clientId == null) throw new Exception("ClientID is null");
      if (clientSecret == null && token == null) throw new Exception("ClientSecret/Token is null");

      IdentificationParams params;
      if (token != null) {
        params = new IdentificationParamsWithToken(
                clientId,
                token,
                themeResource
        );
      } else {
        params = new IdentificationParamsWithClientSecret(
                clientId,
                clientSecret,
                themeResource
        );
        params.setFlowId(flowId);
      }
      this.result = result;
      IdentificationActivity.startForResult(activity,
              REQUEST_CODE_IDENTIFICATION,
              params);

    } catch (Exception e) {
      result.error("AQ_ERROR", e.getMessage(), e.toString());
    }
  }

  void faceAuthentication(MethodCall call, Result result) {
    String verificationId = call.argument("verificationId");
    String clientId = call.argument("clientId");
    String clientSecret = call.argument("clientSecret");
    String token = call.argument("token");
    int themeResource = getStyle(null);
    if (call.argument("theme") != null) {
      Map theme = call.argument("theme");
      if (theme != null && theme.get("AndroidStyle") != null) {
        themeResource = getStyle((String)theme.get("AndroidStyle"));
      }
    }

    try {
      if (verificationId == null) throw new Exception("Verification-ID is null");
      if (clientId == null) throw new Exception("ClientID is null");
      if (clientSecret == null && token == null) throw new Exception("ClientSecret/Token is null");

      FaceAuthenticationParams params;
      if (token != null) {
        params = new FaceAuthenticationParamsWithToken(
                clientId,
                token,
                verificationId,
                themeResource
        );
      } else {
        params = new FaceAuthenticationParamsWithClientSecret(
                clientId,
                clientSecret,
                verificationId,
                themeResource
        );
      }
      this.result = result;
      FaceAuthenticationActivity.startForResult(
              activity,
              REQUEST_CODE_FACE_AUTHENTICATION,
              params
      );
    } catch (Exception e) {
      result.error("AQ_ERROR", e.getMessage(), e.toString());
    }
  }

  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
    this.activity = binding.getActivity();
    binding.addActivityResultListener(this);
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
  }

  @Override
  public void onDetachedFromActivity() {
  }

  @Override
  public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_CODE_IDENTIFICATION) {
      if (resultCode == Activity.RESULT_OK) {
        Map<String, Object> map = new HashMap<>();
        IdentificationResult identificationResult = IdentificationActivity.getResult(data);
        if (identificationResult != null) {
          map.put("verificationId", identificationResult.getVerificationId());
          map.put("selfieImageFilePath", identificationResult.getSelfieImageFilePath());
          map.put("proofOfAddressFilePath", identificationResult.getProofOfAddressFilePath());
          map.put("documents", createDocuments(identificationResult.getDocuments()));
          result.success(map);
        }
      } else {
        final Throwable error = IdentificationActivity.getError(data);
        if (error != null) {
          result.error("AQ_ERROR", error.getMessage(), error.toString());
        } else {
          result.error("AQ_ERROR", "userCanceled", "User canceled flow");
        }
      }
    } else if (requestCode == REQUEST_CODE_FACE_AUTHENTICATION) {
      if (resultCode == Activity.RESULT_OK) {
        Map<String, Object> map = new HashMap<>();
        String code = FaceAuthenticationActivity.getResult(data);
        if (code != null) {
          map.put("code", code);
          result.success(map);
        }
      } else {
        final Throwable error = FaceAuthenticationActivity.getError(data);
        if (error != null) {
          result.error("AQ_ERROR", error.getMessage(), error.toString());
        } else {
          result.error("AQ_ERROR", "userCanceled", "User canceled flow");
        }
      }
    }
    return true;
  }

  // Private

  private int getStyle(String styleName) {
    if (styleName == null) {
      return R.style.AuthenteqCustom;
    }
    try {
      Field resourceField = R.style.class.getDeclaredField(styleName);
      return resourceField.getInt(resourceField);
    } catch (Exception e) {
      return R.style.AuthenteqCustom;
    }
  }

  private List<Map<String,Object>> createDocuments(List<DocumentIdentificationResult> documents) {
    final List<Map<String,Object>> array = new ArrayList<>();
    for (DocumentIdentificationResult document : documents) {
      if (document instanceof PassportIdentificationResult) {
        array.add(createPassport(((PassportIdentificationResult) document)));
      } else if (document instanceof IdCardIdentificationResult) {
        array.add(createIdCard(((IdCardIdentificationResult) document)));
      } else if (document instanceof DriversLicenseIdentificationResult) {
        array.add(createDriversLicense(((DriversLicenseIdentificationResult) document)));
      }
    }
    return array;
  }

  private Map<String,Object> createDriversLicense(DriversLicenseIdentificationResult document) {
    final Map<String,Object> map = new HashMap<>();
    map.put("givenNames", document.getGivenNames());
    map.put("surname", document.getSurname());
    map.put("surnameAndGivenNames", document.getSurnameAndGivenNames());
    final LocalDate dateOfBirth = document.getDateOfBirth();
    map.put("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
    map.put("sex", document.getSex() == null ? null : document.getSex().toString());
    final LocalDate dateOfExpiry = document.getDateOfExpiry();
    map.put("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
    final LocalDate dateOfIssue = document.getDateOfIssue();
    map.put("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
    map.put("issuingCountry", document.getIssuingCountry());
    map.put("documentNumber", document.getDocumentNumber());
    map.put("documentType", document.getDocumentType().toString());
    map.put("address", document.getAddress());
    map.put("placeOfBirth", document.getPlaceOfBirth());
    map.put("issuingAuthority", document.getIssuingAuthority());
    map.put("personalNumber", document.getPersonalNumber());
    map.put("givenNamesLocalized", document.getGivenNamesLocalized());
    map.put("surnameAndGivenNamesLocalized", document.getSurnameAndGivenNamesLocalized());
    map.put("placeOfBirthLocalized", document.getPlaceOfBirthLocalized());
    map.put("issuingAuthorityLocalized", document.getIssuingAuthorityLocalized());
    map.put("addressLocalized", document.getAddressLocalized());
    map.put("jurisdiction", document.getJurisdiction());
    map.put("licenseClass", document.getLicenseClass());
    final Map<String, DriversLicenseIdentificationResult.LicenseClassDetails> licenseClassDetails = document.getLicenseClassDetails();
    final Map<String, Map<String,String>> licenseClassDetailsMap = new HashMap<>();
    for (Map.Entry<String, DriversLicenseIdentificationResult.LicenseClassDetails> entry : licenseClassDetails.entrySet()) {
      final Map<String,String> entryMap = new HashMap<>();
      final DriversLicenseIdentificationResult.LicenseClassDetails classDetails = entry.getValue();
      final LocalDate from = classDetails.getFrom();
      if (from != null) entryMap.put("from", from.toString());
      final LocalDate to = classDetails.getTo();
      if (to != null) entryMap.put("to", to.toString());
      final String notes = classDetails.getNotes();
      if (notes != null) entryMap.put("notes", notes);
      licenseClassDetailsMap.put(entry.getKey(), entryMap);
    }
    map.put("licenseClassDetails", licenseClassDetailsMap);
    putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
    putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
    putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
    map.put("documentFrontImageFilePath", document.getDocumentFrontImageFilePath());
    map.put("documentBackImageFilePath", document.getDocumentBackImageFilePath());
    return map;
  }

  private Map<String,Object> createIdCard(IdCardIdentificationResult document) {
    final Map<String,Object> map = new HashMap<>();
    map.put("givenNames", document.getGivenNames());
    map.put("surname", document.getSurname());
    map.put("surnameAndGivenNames", document.getSurnameAndGivenNames());
    final LocalDate dateOfBirth = document.getDateOfBirth();
    map.put("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
    final LocalDate dateOfExpiry = document.getDateOfExpiry();
    map.put("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
    final LocalDate dateOfIssue = document.getDateOfIssue();
    map.put("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
    map.put("sex", document.getSex() == null ? null : document.getSex().toString());
    map.put("nationality", document.getNationality());
    map.put("issuingCountry", document.getIssuingCountry());
    map.put("documentNumber", document.getDocumentNumber());
    map.put("documentType", document.getDocumentType().toString());
    map.put("address", document.getAddress());
    map.put("placeOfBirth", document.getPlaceOfBirth());
    map.put("issuingAuthority", document.getIssuingAuthority());
    map.put("personalNumber", document.getPersonalNumber());
    map.put("givenNamesLocalized", document.getGivenNamesLocalized());
    map.put("surnameAndGivenNamesLocalized", document.getSurnameAndGivenNamesLocalized());
    map.put("placeOfBirthLocalized", document.getPlaceOfBirthLocalized());
    map.put("issuingAuthorityLocalized", document.getIssuingAuthorityLocalized());
    map.put("addressLocalized", document.getAddressLocalized());
    putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
    putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
    putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
    map.put("documentFrontImageFilePath", document.getDocumentFrontImageFilePath());
    map.put("documentBackImageFilePath", document.getDocumentBackImageFilePath());
    return map;
  }

  private Map<String,Object> createPassport(PassportIdentificationResult document) {
    final Map<String,Object> map = new HashMap<>();
    map.put("givenNames", document.getGivenNames());
    map.put("surname", document.getSurname());
    map.put("surnameAndGivenNames", document.getSurnameAndGivenNames());
    final LocalDate dateOfBirth = document.getDateOfBirth();
    map.put("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
    final LocalDate dateOfExpiry = document.getDateOfExpiry();
    map.put("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
    final LocalDate dateOfIssue = document.getDateOfIssue();
    map.put("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
    map.put("sex", document.getSex() == null ? null : document.getSex().toString());
    map.put("nationality", document.getNationality());
    map.put("issuingCountry", document.getIssuingCountry());
    map.put("documentNumber", document.getDocumentNumber());
    map.put("documentType", document.getDocumentType().toString());
    map.put("address", document.getAddress());
    map.put("placeOfBirth", document.getPlaceOfBirth());
    map.put("issuingAuthority", document.getIssuingAuthority());
    map.put("personalNumber", document.getPersonalNumber());
    map.put("givenNamesLocalized", document.getGivenNamesLocalized());
    map.put("surnameAndGivenNamesLocalized", document.getSurnameAndGivenNamesLocalized());
    map.put("placeOfBirthLocalized", document.getPlaceOfBirthLocalized());
    map.put("issuingAuthorityLocalized", document.getIssuingAuthorityLocalized());
    map.put("addressLocalized", document.getAddressLocalized());
    putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
    putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
    putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
    map.put("documentFrontImageFilePath", document.getDocumentImageFilePath());
    return map;
  }

  private void putBoolean(Map<String,Object> map, String key, Boolean value) {
    map.put(key, value);
  }
}
