## 1.75.1

Fix iOS Face Authentication flow

## 1.75.0

Update to Authenteq Flow SDK 1.75.0

## 1.74.0

Update to Authenteq Flow SDK 1.74.0

## 1.73.0

Update to Authenteq Flow SDK 1.73.0

## 1.72.0

Update to Authenteq Flow SDK 1.72.0

## 1.71.0

Update to Authenteq Flow SDK 1.71.0

## 1.70.0

Update to Authenteq Flow SDK 1.70.0

## 1.69.0

Update to Authenteq Flow SDK 1.69.0

For more details check https://docs.authenteq.com/
