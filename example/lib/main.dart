import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:authenteq_flow/authenteq_flow.dart';
import 'package:authenteq_flow/models/IdentificationParameters.dart';
import 'package:authenteq_flow/models/IdentificationResult.dart';
import 'package:authenteq_flow/models/FaceAuthenticationParameters.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _version = 'Unknown';
  dynamic _identificationResult;
  String? _faceAuthResult;
  Exception? _exception;

  final String _clientId = '< MY CLIENT ID >';
  final String _clientSecret = '< MY SECRET KEY >';

  @override
  void initState() {
    super.initState();
    loadSdkVersion();
  }

  Future<void> loadSdkVersion() async {
    String version;
    try {
      version = await AuthenteqFlow.getVersion;
    } on Exception {
      version = 'Failed to get SDK version.';
    }

    if (!mounted) return;

    setState(() {
      _version = version;
    });
  }

  Future<void> startIdentification() async {
    IdentificationResult? result;
    Exception? exception;
    try {
      IdentificationParameters parameters = IdentificationParameters();
      parameters.clientId = _clientId;
      parameters.clientSecret = _clientSecret;
      // parameters.flowId = 'default-kyc-mobile'; // optional
      parameters.theme = {
        'primaryColor': '#00a2ff',
        'AndroidStyle': 'AuthenteqCustom',
        'identificationInstructionImageForDriverLicense': 'graphics/driver.png'
      };
      result = await AuthenteqFlow.identification(parameters);
    } on Exception catch(e) {
      exception = e;
    }

    setState(() {
      _identificationResult = result;
      _exception = exception;
    });
  }

  Future<void> faceAuthentication() async {
    String? result;
    Exception? exception;
    try {
      FaceAuthenticationParameters parameters = FaceAuthenticationParameters();
      parameters.clientId = _clientId;
      parameters.clientSecret = _clientSecret;
      parameters.theme = {
        'primaryColor': '#00a2ff',
        'AndroidStyle': 'AuthenteqCustom'
      };
      parameters.verificationId = _identificationResult?.verificationId;
      result = await AuthenteqFlow.faceAuthentication(parameters);
    } on Exception catch(e) {
      exception = e;
    }

    setState(() {
      if (result != null) {
        checkFaceAuthentication(result);
      }
      _exception = exception;
    });
  }

  Future<void> checkFaceAuthentication(String code) async {
    String? contentsResult;
    Exception? exception;
    try {
      final basicAuth = base64.encode(utf8.encode('$_clientId:$_clientSecret'));
      final queryParameters = { 'code': code };
      final uri = Uri.https('api.app.authenteq.com', '/mobile-sdk/face-authentication-result', queryParameters);
      final request = await HttpClient().getUrl(uri);
      request.headers.set(HttpHeaders.authorizationHeader, 'Basic $basicAuth');
      var response = await request.close();
      await for (var contents in response.transform(const Utf8Decoder())) {
        var parsedJson = jsonDecode(contents);
        contentsResult = parsedJson['success'].toString().toUpperCase();
      }
    } on Exception catch(e) {
      exception = e;
    }

    setState(() {
      _faceAuthResult = contentsResult;
      _exception = exception;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Authenteq Flow Plugin example'),
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Spacer(),
              ElevatedButton(
                onPressed: () {
                  startIdentification();
                },
                child: const Text('Start Identification'),
              ),
              ElevatedButton(
                onPressed: () {
                  faceAuthentication();
                },
                child: const Text('Face Authentication'),
              ),
              Text('Running with Authenteq SDK: $_version\n'),
              const Spacer(),
              exceptionWidget(),
              identificationResultWidget(),
              faceAuthResultWidget()
            ],
          ),
        ),
      ),
    );
  }

  Widget exceptionWidget() {
    return _exception == null ? Container() : Text(_exception.toString(), style: const TextStyle(color: Colors.red));
  }

  Widget identificationResultWidget() {
    return _identificationResult == null ? Container() : Text(
        "Verification ID: ${_identificationResult.verificationId}\n"
    );
  }

  Widget faceAuthResultWidget() {
    return _faceAuthResult == null ? Container() : Text(
        "Face Authentication Success: $_faceAuthResult\n"
    );
  }
}
