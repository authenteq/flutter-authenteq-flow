#import "AuthenteqFlowPlugin.h"
#if __has_include(<authenteq_flow/authenteq_flow-Swift.h>)
#import <authenteq_flow/authenteq_flow-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "authenteq_flow-Swift.h"
#endif

@implementation AuthenteqFlowPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAuthenteqFlowPlugin registerWithRegistrar:registrar];
}
@end
