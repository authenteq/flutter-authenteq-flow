import Flutter
import UIKit
import AuthenteqFlow

public class SwiftAuthenteqFlowPlugin: NSObject, FlutterPlugin {
  
  private static var registrar: FlutterPluginRegistrar?
  private var callResult: FlutterResult?
  private var authenteqViewController: UIViewController?
  
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "authenteq_flow", binaryMessenger: registrar.messenger())
    let instance = SwiftAuthenteqFlowPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
    SwiftAuthenteqFlowPlugin.registrar = registrar
  }
  
  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch call.method {
    case "getVersion":
      getVersion(result: result)
    case "identification":
      identification(call, result: result)
    case "faceAuthentication":
      faceAuthentication(call, result: result)
    default:
      result(flutterError(from: .invalidConfiguration))
    }
  }
  
  func getVersion(result: FlutterResult) {
    let versionString = Bundle(for: AuthenteqFlow.self).infoDictionary?["CFBundleShortVersionString"] as? String ?? "?"
    result(versionString)
  }

  func identification(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    guard
      let arguments = call.arguments as? [String:Any]
    else {
      result(flutterError(from: .invalidConfiguration))
      return
    }
    let clientSecret = arguments["clientSecret"] as? String
    let flowId = arguments["flowId"] as? String
    let token = arguments["token"] as? String
    guard
      let clientId = arguments["clientId"] as? String,
      (clientSecret != nil || token != nil)
    else {
      result(flutterError(from: .invalidConfiguration))
      return
    }
    self.callResult = result
    
    DispatchQueue.main.async {
      if let theme = arguments["theme"] as? [String: Any] {
        self.applyCustomTheme(theme)
      }
      let identificationParams: IdentificationParams
      if let clientSecret = clientSecret {
        identificationParams = IdentificationParams(
          clientId: clientId,
          clientSecret: clientSecret
        )
        identificationParams.flowId = flowId
      } else {
        identificationParams = IdentificationParams(
          clientId: clientId,
          token: token ?? ""
        )
      }
      let flowViewController = AuthenteqFlow.instance.identificationViewController(
        with: identificationParams,
        delegate: self
      )
      self.authenteqViewController = flowViewController
      self.authenteqViewController?.modalPresentationStyle = .fullScreen
      UIApplication.shared.keyWindow?.rootViewController?.present(flowViewController, animated: true)
    }
  }
  
  func faceAuthentication(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    guard
      let arguments = call.arguments as? [String:Any]
    else {
      result(flutterError(from: .invalidConfiguration))
      return
    }
    let clientSecret = arguments["clientSecret"] as? String
    let token = arguments["token"] as? String
    guard
      let verificationId = arguments["verificationId"] as? String,
      let clientId = arguments["clientId"] as? String,
      (clientSecret != nil || token != nil)
    else {
      result(flutterError(from: .invalidConfiguration))
      return
    }
    self.callResult = result
    
    DispatchQueue.main.async {
      if let theme = arguments["theme"] as? [String: Any] {
        self.applyCustomTheme(theme)
      }
      let faceAuthenticationParams: FaceAuthenticationParams
      if let clientSecret = clientSecret {
        faceAuthenticationParams = FaceAuthenticationParams(
          clientId: clientId,
          clientSecret: clientSecret,
          verificationId: verificationId
        )
      } else {
        faceAuthenticationParams = FaceAuthenticationParams(
          clientId: clientId,
          token: token ?? "",
          verificationId: verificationId
        )
      }
      let flowViewController = AuthenteqFlow.instance.faceAuthenticationViewController(
        with: faceAuthenticationParams,
        delegate: self
      )
      self.authenteqViewController = flowViewController
      self.authenteqViewController?.modalPresentationStyle = .fullScreen
      UIApplication.shared.keyWindow?.rootViewController?.present(flowViewController, animated: true)
    }
  }
  // MARK: - Private Methods
  
  private func convertImage(_ value: Any?) -> UIImage? {
    if
      let value = value as? String,
      let key = SwiftAuthenteqFlowPlugin.registrar?.lookupKey(forAsset: value),
      let path = Bundle.main.path(forResource: key, ofType: nil)
    {
      return UIImage(contentsOfFile: path)
    } else {
      return nil
    }
  }
  
  private func toDictionary(result: IdentificationResult) -> [String: Any] {
    var dictionary = [String: Any]()
    dictionary["verificationId"] = result.verificationId
    dictionary["selfieImageFilePath"] = result.selfieImageFilePath
    dictionary["proofOfAddressFilePath"] = result.proofOfAddressFilePath
    dictionary["documents"] = result.documents.map { toDictionary(document: $0) }
    return dictionary
  }
  
  private func toDictionary(document: DocumentIdentificationResult) -> [String: Any] {
    switch document {
    case let document as PassportIdentificationResult:
      return toDictionary(passport: document)
    case let document as IdCardIdentificationResult:
      return toDictionary(idCard: document)
    case let document as DriversLicenseIdentificationResult:
      return toDictionary(driversLicense: document)
    default:
      return [:]
    }
  }
  
  private func toDictionary(passport document: PassportIdentificationResult) -> [String: Any] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentFrontFilePath"] = document.documentImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["surname"] = document.surname
    dictionary["surnameAndGivenNames"] = document.surnameAndGivenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["nationality"] = document.nationality
    dictionary["sex"] = document.sex
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    dictionary["address"] = document.address
    dictionary["givenNamesLocalized"] = document.givenNamesLocalized
    dictionary["surnameAndGivenNamesLocalized"] = document.surnameAndGivenNamesLocalized
    dictionary["placeOfBirthLocalized"] = document.placeOfBirthLocalized
    dictionary["issuingAuthorityLocalized"] = document.issuingAuthorityLocalized
    dictionary["addressLocalized"] = document.addressLocalized
    return dictionary
  }
  
  private func toDictionary(idCard document: IdCardIdentificationResult) -> [String: Any] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentBackFilePath"] = document.documentBackImageFilePath
    dictionary["documentFrontFilePath"] = document.documentFrontImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["surname"] = document.surname
    dictionary["surnameAndGivenNames"] = document.surnameAndGivenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["nationality"] = document.nationality
    dictionary["sex"] = document.sex
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    dictionary["address"] = document.address
    dictionary["givenNamesLocalized"] = document.givenNamesLocalized
    dictionary["surnameAndGivenNamesLocalized"] = document.surnameAndGivenNamesLocalized
    dictionary["placeOfBirthLocalized"] = document.placeOfBirthLocalized
    dictionary["issuingAuthorityLocalized"] = document.issuingAuthorityLocalized
    dictionary["addressLocalized"] = document.addressLocalized
    return dictionary
  }
  
  private func toDictionary(driversLicense document: DriversLicenseIdentificationResult) -> [String: Any] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentBackFilePath"] = document.documentBackImageFilePath
    dictionary["documentFrontFilePath"] = document.documentFrontImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["surname"] = document.surname
    dictionary["surnameAndGivenNames"] = document.surnameAndGivenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["sex"] = document.sex
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    dictionary["address"] = document.address
    dictionary["givenNamesLocalized"] = document.givenNamesLocalized
    dictionary["surnameAndGivenNamesLocalized"] = document.surnameAndGivenNamesLocalized
    dictionary["placeOfBirthLocalized"] = document.placeOfBirthLocalized
    dictionary["issuingAuthorityLocalized"] = document.issuingAuthorityLocalized
    dictionary["addressLocalized"] = document.addressLocalized
    dictionary["jurisdiction"] = document.jurisdiction
    dictionary["licenseClass"] = document.licenseClass
    let licenseClassDetail: [String: Any]? = document.licenseClassDetails?.mapValues({ (detail) in
      var dictionaryDetail = [String: Any]()
      dictionaryDetail["from"] = dateFormatter.string(fromOptional: detail.from)
      dictionaryDetail["to"] = dateFormatter.string(fromOptional: detail.to)
      dictionaryDetail["notes"] = detail.notes
      return dictionaryDetail
    })
    dictionary["licenseClassDetails"] = licenseClassDetail
    return dictionary
  }
  
  private func errorDescription(from error: AuthenteqFlowError) -> String {
    switch error {
    case .authenteqLicenseError:
      return "License error"
    case .cancelledByUser:
      return "User cancelled flow"
    case .ocrSetupError:
      return "Setup error"
    case .jailbreakDetected:
      return "Device jailbreak detected"
    case .internalSetupError:
      return "Setup error"
    case .livenessSetupError:
      return "Setup error"
    case .connectionCertificateError:
      return "Unsecure internet connection"
    case .invalidConfiguration:
      return "Configuration not allowed"
    case .invalidAuthToken:
      return "Invalid authentication token"
    case .requiredAgeNotMet:
      return "Required age not met"
    @unknown default:
      return "Unknown Error"
    }
  }
  
  private func errorCode(from error: AuthenteqFlowError) -> String {
    switch error {
    case .authenteqLicenseError:
      return "authenteqLicenseError"
    case .cancelledByUser:
      return "userCancelled"
    case .ocrSetupError:
      return "ocrSetupError"
    case .jailbreakDetected:
      return "jailbreakDetected"
    case .internalSetupError:
      return "internalSetupError"
    case .livenessSetupError:
      return "livenessSetupError"
    case .connectionCertificateError:
      return "connectionCertificateError"
    case .invalidConfiguration:
      return "invalidConfiguration"
    case .invalidAuthToken:
      return "invalidAuthToken"
    case .requiredAgeNotMet:
      return "requiredAgeNotMet"
    @unknown default:
      return "unknownError"
    }
  }
  
  private func flutterError(from error: AuthenteqFlowError) -> FlutterError {
    return FlutterError(
      code: "AQ_ERROR",
      message: errorDescription(from: error),
      details: nil
    )
  }
  
  private func closeAuthenteqFlow() {
    authenteqViewController?.dismiss(animated: true)
    authenteqViewController = nil
    callResult = nil
  }
  
  private func applyCustomTheme(_ values: [String: Any]) {
    let theme = AuthenteqFlow.instance.theme

    // Colors
    if let primaryColor = UIColor(hexString: values["primaryColor"] as? String ?? "") {
      theme.primaryColor = primaryColor
    }
    if let textColor = UIColor(hexString: values["textColor"] as? String ?? "") {
      theme.textColor = textColor
    }
    if let screenBackgroundColor = UIColor(hexString: values["screenBackgroundColor"] as? String ?? "") {
      theme.screenBackgroundColor = screenBackgroundColor
    }
    if let viewBackgroundHightlightColor = UIColor(hexString: values["viewBackgroundHightlightColor"] as? String ?? "") {
      theme.viewBackgroundHightlightColor = viewBackgroundHightlightColor
    }
    if let separatorColor = UIColor(hexString: values["separatorColor"] as? String ?? "") {
      theme.separatorColor = separatorColor
    }
    if let selectedButtonTextColor = UIColor(hexString: values["selectedButtonTextColor"] as? String ?? "") {
      theme.selectedButtonTextColor = selectedButtonTextColor
    }
    if let selectedButtonBackgroundColor = UIColor(hexString: values["selectedButtonBackgroundColor"] as? String ?? "") {
      theme.selectedButtonBackgroundColor = selectedButtonBackgroundColor
    }

    // Fonts
    if let font = UIFont(name: values["font"] as? String ?? "", size: 16) {
      theme.font = font
    }
    if let boldFont = UIFont(name: values["boldFont"] as? String ?? "", size: 16) {
      theme.boldFont = boldFont
    }

    // Images
    theme.identificationInstructionImageForSelfie = self.convertImage(values["identificationInstructionImageForSelfie"])
    theme.identificationInstructionImageForPassport = self.convertImage(values["identificationInstructionImageForPassport"])
    theme.identificationInstructionImageForDriverLicense = self.convertImage(values["identificationInstructionImageForDriverLicense"])
    theme.identificationInstructionImageForIdCard = self.convertImage(values["identificationInstructionImageForIdCard"])
    theme.identificationNfcSymbolCheck = self.convertImage(values["identificationNfcSymbolCheck"])
    theme.identificationNfcInstruction = self.convertImage(values["identificationNfcInstruction"])
    theme.identificationNfcScanInside = self.convertImage(values["identificationNfcScanInside"])
    theme.identificationIconForIdDocument = self.convertImage(values["identificationIconForIdDocument"])
    theme.identificationIconForProofOfAddress = self.convertImage(values["identificationIconForProofOfAddress"])
  }
}

// MARK: - AuthenteqIdentificationDelegate

extension SwiftAuthenteqFlowPlugin: AuthenteqIdentificationDelegate {
  
  public func authenteqDidFinishIdentification(with result: IdentificationResult) {
    let dictionary = toDictionary(result: result)
    callResult?(dictionary)
    closeAuthenteqFlow()
  }
  
  public func authenteqDidFailIdentification(with error: AuthenteqFlowError) {
    callResult?(flutterError(from: error))
    closeAuthenteqFlow()
  }
}

// MARK: - AuthenteqFaceAuthenticationDelegate

extension SwiftAuthenteqFlowPlugin: AuthenteqFaceAuthenticationDelegate {
  
  public func authenteqDidFinishFaceAuthentication(with code: String) {
    callResult?(["code": code])
    closeAuthenteqFlow()
  }
  
  public func authenteqDidFailFaceAuthentication(with error: AuthenteqFlowError) {
    callResult?(flutterError(from: error))
    closeAuthenteqFlow()
  }
}

// MARK: - UIColor extension

fileprivate extension UIColor {
  
  convenience init?(hexString: String) {
    var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
    switch chars.count {
    case 3: chars = chars.flatMap { [$0, $0] }; fallthrough
    case 6: chars = ["F","F"] + chars
    case 8: break
    default: return nil
    }
    self.init(
      red: .init(strtoul(String(chars[2...3]), nil, 16)) / 255,
      green: .init(strtoul(String(chars[4...5]), nil, 16)) / 255,
      blue: .init(strtoul(String(chars[6...7]), nil, 16)) / 255,
      alpha: .init(strtoul(String(chars[0...1]), nil, 16)) / 255)
  }
  
  convenience init?(hex: Int, transparency: CGFloat = 1) {
    let red = CGFloat((hex >> 16) & 0xFF)
    let green = CGFloat((hex >> 8) & 0xFF)
    let blue = CGFloat(hex & 0xFF)
    self.init(
      red: red / 255.0,
      green: green / 255.0,
      blue: blue / 255.0,
      alpha: min(max(transparency, 0), 1)
    )
  }
}

// MARK: - DateFormatter extension

fileprivate extension DateFormatter {
  
  func string(fromOptional date: Date?) -> String? {
    if let date = date {
      return string(from: date as Date)
    } else {
      return nil
    }
  }
}

