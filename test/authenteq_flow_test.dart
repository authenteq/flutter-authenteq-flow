import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:authenteq_flow/authenteq_flow.dart';

void main() {
  const MethodChannel channel = MethodChannel('authenteq_flow');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getVersion', () async {
    expect(await AuthenteqFlow.getVersion, '42');
  });
}
